var AWS = require('aws-sdk')

var handler = function(e, context, callback) {
  var dynamodb = new AWS.DynamoDB({
    apiVersion: '2012-08-10', //obligatorio
    endpoint: 'http://dynamodb:8000',
    region: 'us-west-2', //obligatorio
    credentials: { //obligatorio solo en local porque en produccion ya la tiene
      accessKeyId: '2345',
      secretAccessKey: '2345'
    }
  });

  var docClient = new AWS.DynamoDB.DocumentClient({
    apiVersion: '2012-08-10', //obligatorio
    service: dynamodb
  });

  //Mi codigo
  //Listar tablas
  // dynamodb.listTables({}, function(err, data) {
  //   if (err) console.log(err); // an error occurred
  //   else callback(null, { body: JSON.stringify(data) }); // successful response
  // });
  let idEnvio = (e.pathParameters || {}).idEnvio || false;
  switch(e.httpMethod) {
    case "GET":
      switch (e.resource) {
        case '/envios/{idEnvio}':
          var params = {
             TableName: 'Envio',
             KeyConditionExpression: 'id = :value',
             ExpressionAttributeValues: {
               ':value': idEnvio.toString(),
             },
          }
          docClient.query(params, function(err, data) {
            if (err) callback(null, { statusCode: 500, body: JSON.stringify(err) })
            else callback(null, { statusCode: 201, body: JSON.stringify(data) })
          })
          break;
        case '/envios/pendientes':
          var params = {
             TableName: 'Envio',
             IndexName: 'EnviosPendientesIndex',
          }
          docClient.scan(params, function(err, data) {
            if (err) callback(null, { statusCode: 500, body: JSON.stringify(err) })
            else callback(null, { statusCode: 201, body: JSON.stringify(data) })
          })
          break;
        default:
          var params = {
             TableName: 'Envio'
          };
          docClient.scan(params, function(err, data) {
            console.log(data)
            if (err) callback(null, { statusCode: 500, body: JSON.stringify(err) })
            else callback(null, { statusCode: 201, body: JSON.stringify(data) })
          });
      }
      break;
    case "POST":
      let body = JSON.parse(e.body)
      switch (e.resource) {
        case '/envios': // CREAR
          let envio_item = body
          envio_item.fechaAlta = new Date().toISOString();
          envio_item.pendiente = envio_item.fechaAlta;
          envio_item.id = guid();

          var params = {
             TableName: 'Envio',
             Item: envio_item
          };
          docClient.put(params, function(err, data) {
            if (err) callback(null, { statusCode: 500, body: JSON.stringify(err) })
            else callback(null, { statusCode: 201, body: JSON.stringify(envio_item) })
          })
          break;
        case '/envios/{idEnvio}/movimiento':
          var params = {
              TableName: 'Envio',
              Key: { id: idEnvio },
          };
          docClient.get(params, function(err, data) {
            let envio_item = data.Item
            let movimiento = body
            if(!envio_item.historial) envio_item.historial = []
            movimiento.fecha = new Date().toISOString()
            envio_item.historial.push(movimiento)
            var params = {
               TableName: 'Envio',
               Item: envio_item
            };
            docClient.put(params, function(err, data) {
              if (err) callback(null, { statusCode: 500, body: JSON.stringify(err) })
              else callback(null, { statusCode: 201, body: JSON.stringify(envio_item) })
            })
          })
          break;
        case '/envios/{idEnvio}/entregado':
          var params = {
              TableName: 'Envio',
              Key: { id: idEnvio },
          };
          docClient.get(params, function(err, data) {
            let envio_item = data.Item
            //Aca no se si tengo que buscar por pendiente y si no trae nada avisar,
            //o tengo que buscar por HASH y borrar el atributo pendiente y ver que no falle
            delete envio_item.pendiente
            var params = {
               TableName: 'Envio',
               Item: envio_item
            };
            docClient.put(params, function(err, data) {
              if (err) callback(null, { statusCode: 500, body: JSON.stringify(err) })
              else callback(null, { statusCode: 201, body: JSON.stringify(envio_item) })
            })
          })
          break;
        default:
      }
      break;
    default:
      console.log("Metodo no soportado (" + e.httpMethod + ")");
      callback(null, { statusCode: 501 });
  }
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

exports.handler = handler;
